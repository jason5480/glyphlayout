!(function() {
    "use strict"

    // Get the main svg element as defined in html
    var svg = d3.select("svg"),
        svgWidth = +svg.attr("width"),
        svgHeight = +svg.attr("height");

    // Set the margins and the active width ans height
    var margin = {top: 0, right: 0, bottom: 0, left: 0},
        width = svgWidth - margin.left - margin.right,
        height = svgHeight - margin.top - margin.bottom;

    /*
     * value accessor - returns the value to encode for a given data object.
     * scale - maps value to a visual display encoding, such as a pixel position.
     * map function - maps from data value to display value
     * axis - sets up axis
     */

    // Set glyph properties
    var glyphSize   = height/28,
        glyphRmax   = glyphSize/2,
        glyphRmin   = glyphRmax/3;

    var rScale = d3.scaleLinear().range([glyphRmin, glyphRmax]);
    var cScale = d3.scaleLinear();

    // setup x
    var xValue = (d) => { return d.x;}, // data -> value
        xScale = d3.scaleLinear().range([glyphRmax, width - glyphRmax]), // value -> display
        xMap = (d) => { return xValue(d);}, // data -> display
        xAxis = d3.axisTop(xScale)
                  .ticks((width + 2) / (height + 2) * 10)
                  .tickSize(height)
                  .tickPadding(8 - height);

    // setup y
    var yValue = (d) => { return d.y;}, // data -> value
        yScale = d3.scaleLinear().range([height - glyphRmax, glyphRmax]), // value -> display
        yMap = (d) => { return yValue(d);}, // data -> display
        yAxis = d3.axisRight(yScale)
                  .ticks(10)
                  .tickSize(width)
                  .tickPadding(8 - width);

    // setup fill color
    var cValue = (d) => { return d.species},
        color = d3.scaleOrdinal(d3.schemeCategory10);

    // Set radial line for chord points
    var line = d3.line()
                 .curve(d3.curveBasisClosed)
                 .x( (p) => {return p[0]})
                 .y( (p) => {return p[1]});

    // Set zoom behaviour
    var zoom = d3.zoom()
                 .scaleExtent([1, 20])
                 .translateExtent([[-width/2, -height/2], [3 * width / 2, 3 * height / 2]])
                 .on("zoom", zoomed);

    //append clip path for lines plotted, hiding those part out of bounds
    var clip = svg.append("defs")
                  .append("svg:clipPath")
                  .attr("id", "clip")
                  .append("svg:rect")
                  .attr("id", "clip-rect")
                  .attr("width", svgWidth)
                  .attr("height", svgHeight);

    // Add the zoomable area
    var container = svg.append("g")
                       .attr("class", "container")
                       .attr("clip-path", "url(#clip)"); //use clip path to make irrelevant part invisible

    // Draw a rect to see the the interactive area and append clip path for glyph parts out of bounds
    var rect = container.append("svg:rect")
                        .attr("width", svgWidth)
                        .attr("height", svgHeight)
                        .style("fill", "none")
                        .style("stroke-width", 1)
                        .style("stroke", "black");

    var view = container.append("g")
                        .attr("class", "view");

    // x-axis group
    var gX = svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + svgHeight + ")")
                .call(xAxis);

    // y-axis group
    var gY = svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);

    // Create a force simulation to place the data
    var simulation = d3.forceSimulation()
                       .force("link", d3.forceLink().id( (d) => { return d.id; }).distance( (d) => {return d.distance;}))
                       .force("collide",d3.forceCollide( (d) => {return glyphRmax }).iterations(16) )
                       .force("charge", d3.forceManyBody().strength(-100))
                       //.force("center", d3.forceCenter(width / 2, height / 2))
                       .force('x', d3.forceX().strength(0.03).x(width / 2))
                       .force('y', d3.forceY().strength(0.03).y(height / 2))
                       .stop();

    // add the tooltip area to the webpage
    var tooltip = d3.select("body").append("div")
                    .attr("class", "tooltip")
                    .style("opacity", 0);

    // load data
    d3.csv("data/flowers.csv", typeFlowers, (error, data) => {
        if (error) throw error;

        // convert original data into a graph structure
        var graph = convertToGraph(data);
        // Draw the
        //drawGraph(graph);
        //drawLayout(data);
        drawGraphLayout(graph);
    });

    svg.call(zoom);

    function typeFlowers (d) {
        // change string (from CSV) into number format
        return {
            sepalLength: +d["sepal length"],
            sepalWidth: +d["sepal width"],
            petalLength: +d["petal length"],
            petalWidth: +d["petal width"],
            species: d.species
        };
    };

    function convertToGraph (data) {
        /*
         * get the numeric traits of the data set and calculate basic metrics
         */

        var dimensions = [],
            summaries = {mean: {}, median: {}, domain: {}, dom: {}, coeff: {}},
            nodes = [],
            links = [];

        dimensions = d3.keys(data[0]).filter( (dim) => {
            return dim !== 'species'
                && ( summaries.mean[dim] = d3.mean(data, (d) => { return d[dim]; }) )
                && ( summaries.median[dim] = d3.median(data, (d) => { return d[dim]; }) )
                && ( summaries.domain[dim] = d3.extent(data, (d) => {
                    d[dim] = ( (d[dim] == '') ? summaries.median[dim] : +d[dim]);
                    return d[dim];
                }) );
        });
        var n = dimensions.length;
        var m = data.length;

        cScale.range([(glyphRmin - glyphRmax)/n, (glyphRmax - glyphRmin)/n]);

        // Find symmetrical domain around mean
        dimensions.forEach( (dim) => {
            var rangeAbove = summaries.domain[dim][1] - summaries.mean[dim];
            var rangeBelow = summaries.mean[dim] - summaries.domain[dim][0];
            summaries.dom[dim] = (rangeAbove > rangeBelow) ?
                [summaries.mean[dim] - rangeAbove, summaries.mean[dim] + rangeAbove] :
                [summaries.mean[dim] - rangeBelow, summaries.mean[dim] + rangeBelow];
        });

        summaries.coeff = coeffArray(n);
        summaries.lcm = leastCommonMultiple(1, summaries.coeff.max);

        var D = data.map( (d1, i) => {
            var dist = [];
            data.forEach( (d2, j) => {
                dist.push(distance(d1, d2, dimensions));
            })
            return dist;
        });

        var positions = mds(D);
        xScale.domain(d3.extent(positions, (p) => {return p[0];}));
        yScale.domain(d3.extent(positions, (p) => {return p[1];}));
        data.forEach( (d,i) => {
            var node = {};
            node.id = i;
            node.x = xScale(positions[i][0]);
            node.y = yScale(positions[i][1]);
            node.group = d.species;
            node.species = d.species;
            dimensions.forEach( (dim) => {
                node[dim] = data[i][dim];
            })
            nodes[i] = node;
        });

        var k = 0;
        for (var i = 0; i < m; ++i) {
            for (var j = 0; j < m; ++j) {
                if (i < j) {
                    var link = {};
                    link.source = i;
                    link.target = j;
                    link.distance = 100 * D[i][j];
                    links[k++] = link;
                }
            }
        }

        return {
            dimensions: dimensions,
            summaries: summaries,
            nodes: nodes,
            links: links
        };
    }

    function drawGraph(graph) {
        var link = view.append("g")
                      .attr("class", "links")
                      .selectAll("line")
                      .data(graph.links)
                      .enter().append("line")
                      .attr("stroke-width", function(d) { return Math.sqrt(d.value); });

        var node = view.append("g")
                      .attr("class", "nodes")
                      .selectAll("circle")
                      .data(graph.nodes)
                      .enter().append("circle")
                      .attr("r", 5)
                      .attr("fill", (d) => { return color(cValue(d)); })
                      .call(d3.drag()
                              .on("start", dragstarted)
                              .on("drag", dragged)
                              .on("end", dragended));

        node.append("title")
            .text(function(d) { return d.id; });

        simulation
            .nodes(graph.nodes)
            .on("tick", ticked);

        simulation.force("link")
                  .links(graph.links);

        function ticked() {
            link
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

            node
                .attr("cx", function(d) { return d.x; })
                .attr("cy", function(d) { return d.y; });
        }

        function dragstarted(d) {
            if (!d3.event.active) simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragended(d) {
            if (!d3.event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }
    }

    function drawGraphLayout (graph) {

        // var node = view.append("g")
        //                .attr("class", "nodes")
        //                .selectAll("circle")
        //                .data(graph.nodes)
        //                .enter().append("circle")
        //                .attr("r", 5)
        //                .attr("fill", (d) => { return color(cValue(d)); })
        //                .call(d3.drag()
        //                        .on("start", dragstarted)
        //                        .on("drag", dragged)
        //                        .on("end", dragended));
        //

        // draw chords
        var chords = view.append("g")
                         .attr("class", "chords")
                         .selectAll('.chord')
                         .data(graph.nodes)
                         .enter()
                         .append('g')
                         .attr('class', 'chord')
                         .call(d3.drag()
                                 .on("start", dragstarted)
                                 .on("drag", dragged)
                                 .on("end", dragended));

        var chord_paths = chords.append('path')
                            .attr('d', chord)
                            .style('fill', (d) => { return color(cValue(d)); });

        var chord_centers = chords.append('circle')
                              .attr('r', 0.5)
                              .attr('cx', (d) => { return xMap(d); } )
                              .attr('cy', (d) => { return yMap(d); } );

        // set tooltip
        chords.on('mouseover', (d) => {
            tooltip.transition()
                   .duration(200)
                   .style('opacity', .9);
            tooltip.html(tooltipHtmlText(d))
                   .style('left', (d3.event.pageX + 5) + 'px')
                   .style('top', (d3.event.pageY - 28) + 'px');
        }).on('mouseout', (d) => {
            tooltip.transition()
                   .duration(500)
                   .style('opacity', 0);
        });

        // chords.append("title")
        //       .text(function(d) { return d.id; });

        // Returns the chord for a given data point.
        function chord(d) {// setup chord
            var pts = d3.range(0, 2 * Math.PI, Math.PI / (2 * graph.summaries.lcm)).map( (t) => {
                var r = (glyphRmax + glyphRmin)/2;
                return [t, r]
            });
            return line(pts.map( (p) => {
                var r = p[1]; // p should not be modified
                graph.dimensions.forEach( (dim, i) => {
                    cScale.domain(graph.summaries.domain[dim]);
                    r += cScale(d[dim]) * Math.sin(graph.summaries.lcm * graph.summaries.coeff.arr[i] * p[0]);
                });
                return [ xMap(d) + r * Math.sin(p[0]), yMap(d) - r * Math.cos(p[0]) ];
            }));
        }

        simulation
            .nodes(graph.nodes)
            .on("tick", ticked);

        simulation.force("link")
                  .links(graph.links);

        function ticked() {
            chord_paths.attr("d", chord);
            chords.selectAll("circle")
                .attr("cx", function (d) {
                    return d.x; })
                .attr("cy", function (d) {
                    return d.y });
        }

        function dragstarted(d) {
            if (!d3.event.active) simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragended(d) {
            if (!d3.event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }

        function tooltipHtmlText(d) {
            var text = "ID: " + ' (' + d['id'] + ')<br/>';
            graph.dimensions.forEach( (dim) => {
                text += dim + ": " + d[dim] + '<br/>';
            });
            return text;
        }

        // Draw axis labels
        svg.append("text")
           .attr("class", "label")
           .attr("transform", "translate(0," + (svgHeight - 15) + ")")
           .attr("x", width)
           .attr("y", -6)
           .style("text-anchor", "end")
           .text("MDS1");

        svg.append("text")
           .attr("class", "label")
           .attr("transform", " translate(20, 0) rotate(-90)")
           .attr("y", 6)
           .attr("dy", ".71em")
           .style("text-anchor", "end")
           .text("MDS2");

        // draw legend
        var legend = svg.selectAll(".legend")
                        .data(color.domain())
                        .enter().append("g")
                        .attr("class", "legend")
                        .attr("transform", (d, i) => { return "translate(0," + i * 20 + ")"; });

        // draw legend colored rectangles
        legend.append("rect")
              .attr("x", svgWidth - 18)
              .attr("width", 18)
              .attr("height", 18)
              .style("fill", color);

        // draw legend text
        legend.append("text")
              .attr("x", svgWidth - 24)
              .attr("y", 9)
              .attr("dy", ".35em")
              .style("text-anchor", "end")
              .text( (d) => { return d;});

        // When the user clicks on the "Advance" button, we
        // start the force layout (The tick handler will stop
        // the layout after one iteration.)

        d3.select('#advance').on('click', function() {
            simulation.tick();
        });

        // When the user clicks on the "Slow Motion" button, we're
        // going to run the force layout until it concludes.

        d3.select('#slow').on('click', function() {

            // Indicate that the animation is in progress.

            simulation.slowMotion = true;
            simulation.fullSpeed  = false;

            // Get the animation rolling

            simulation.alpha(0.5).restart();

        });

        // When the user clicks on the "Slow Motion" button, we're
        // going to run the force layout until it concludes.

        d3.select('#play').on('click', function() {

            // Indicate that the full speed operation is in progress.

            simulation.slowMotion = false;
            simulation.fullSpeed  = true;

            // Get the animation rolling

            simulation.restart();

        });

        // When the user clicks on the "Reset" button, we'll
        // start the whole process over again.

        d3.select('#reset').on('click', function() {

            // If we've already started the layout, stop it.
            if (simulation) {
                simulation.stop();
            }

            // Re-initialize to start over again.
            simulation.alpha(1).restart();
        });
    };

    function drawLayout (data) {
        /*
         * get the numeric traits of the data set and calculate basic metrics
         */
        var meanByDim = {},
            medianByDim = {},
            domainByDim = {},
            domByDim = {};

        var dimensions = d3.keys(data[0]).filter(function (dim) {
            return dim !== 'species'
                && ( meanByDim[dim] = d3.mean(data, function (d) {
                    return +d[dim];
                }) )
                && ( medianByDim[dim] = d3.median(data, function (d) {
                    return +d[dim];
                }) )
                && ( domainByDim[dim] = d3.extent(data, function (d) {
                    d[dim] = ( (d[dim] == '') ? medianByDim[dim] : +d[dim]);
                    return d[dim];
                }) );
        });
        var n = dimensions.length;
        var m = data.length;

        cScale.range([(glyphRmin - glyphRmax)/n, (glyphRmax - glyphRmin)/n]);

        // Find symmetrical domain around mean
        dimensions.forEach(function (dim) {
            var rangeAbove = domainByDim[dim][1] - meanByDim[dim];
            var rangeBelow = meanByDim[dim] - domainByDim[dim][0];
            domByDim[dim] = (rangeAbove > rangeBelow) ?
                [meanByDim[dim] - rangeAbove, meanByDim[dim] + rangeAbove] :
                [meanByDim[dim] - rangeBelow, meanByDim[dim] + rangeBelow];
        });

        var dimCoeff = coeffArray(n);
        var lcm = leastCommonMultiple(1, dimCoeff.max);

        var D = data.map( function (d1, i) {
            var dist = [];
            data.forEach( function (d2, j) {
                dist.push(distance(d1, d2, dimensions));
            })
            return dist;
        })

        var flowerPositions = mds(D);

        data.forEach( function (d,i) {
            d.id = i;
            d.x = flowerPositions[i][0];
            d.y = flowerPositions[i][1];
        })

        // don't want glyphs overlapping axis, so add in buffer to data domain
        xScale.domain(d3.extent(data, xValue));
        yScale.domain(d3.extent(data, yValue));

        // draw chords
        var chords = view.selectAll('.chord')
                         .data(data)
                         .enter()
                         .append('g')
                         .attr('class', 'chord');

        chords.append('path')
              .attr('d', chord)
              .style('fill', function (d) { return color(cValue(d)); });

        chords.append('circle')
              .attr('r', 0.5)
              .attr('cx', function (d) { return xMap(d); } )
              .attr('cy', function (d) { return yMap(d); } );

        // set tooltip
        chords.on('mouseover', function (d) {
            tooltip.transition()
                   .duration(200)
                   .style('opacity', .9);
            tooltip.html(tooltipHtmlText(d))
                   .style('left', (d3.event.pageX + 5) + 'px')
                   .style('top', (d3.event.pageY - 28) + 'px');
        }).on('mouseout', function (d) {
            tooltip.transition()
                   .duration(500)
                   .style('opacity', 0);
        });

        // Returns the chord for a given data point.
        function chord(d) {
            var pts = d3.range(0, 2 * Math.PI, 0.1).map( function(t){
                // set initial radial points in constant radius
                var r = (glyphRmax + glyphRmin)/2;
                // add a sin for each dimension
                dimensions.forEach( function (dim, i) {
                    cScale.domain(domainByDim[dim]);
                    r += cScale(d[dim]) * Math.sin(lcm * dimCoeff.arr[i] * p[0]);
                });
                return [t, r]
            });

            return line(pts.map(function (p) {
                var r = p[1]; // p should not be modified
                // convert from radial to cartesian coords and trannslate
                return [ xMap(d) + r * Math.sin(p[0]), yMap(d) - r * Math.cos(p[0]) ];
            }));
        }

        function tooltipHtmlText(d) {
            var text = "ID: " + ' (' + d['id'] + ')<br/>';
            dimensions.forEach(function (dim) {
                text += dim + ": " + d[dim] + '<br/>';
            });
            return text;
        }

        // Draw axis labels
        svg.append("text")
           .attr("class", "label")
           .attr("transform", "translate(0," + (svgHeight - 15) + ")")
           .attr("x", width)
           .attr("y", -6)
           .style("text-anchor", "end")
           .text("MDS1");

        svg.append("text")
           .attr("class", "label")
           .attr("transform", " translate(20, 0) rotate(-90)")
           .attr("y", 6)
           .attr("dy", ".71em")
           .style("text-anchor", "end")
           .text("MDS2");

        // draw legend
        var legend = svg.selectAll(".legend")
                        .data(color.domain())
                        .enter().append("g")
                        .attr("class", "legend")
                        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

        // draw legend colored rectangles
        legend.append("rect")
              .attr("x", svgWidth - 18)
              .attr("width", 18)
              .attr("height", 18)
              .style("fill", color);

        // draw legend text
        legend.append("text")
              .attr("x", svgWidth - 24)
              .attr("y", 9)
              .attr("dy", ".35em")
              .style("text-anchor", "end")
              .text(function(d) { return d;});
    };

    function zoomed() {
        view.attr("transform", d3.event.transform);
        gX.call(xAxis.scale(d3.event.transform.rescaleX(xScale)));
        gY.call(yAxis.scale(d3.event.transform.rescaleY(yScale)));
    };

    function distance(a, b, dimensions) {
        var sum = 0;
        dimensions.forEach( (dim) => {
            sum +=  Math.pow(a[dim] - b[dim],2)
        })
        var d = Math.pow(sum, 1/dimensions.length);
        return d;
    };

    function mds(distances, dimensions) {
        dimensions = dimensions || 2;

        // square distances
        var M = numeric.mul(-0.5, numeric.pow(distances, 2));

        // double centre the rows/columns
        function mean(A) { return numeric.div(numeric.add.apply(null, A), A.length); }
        var rowMeans = mean(M),
            colMeans = mean(numeric.transpose(M)),
            totalMean = mean(rowMeans);

        for (var i = 0; i < M.length; ++i) {
            for (var j =0; j < M[0].length; ++j) {
                M[i][j] += totalMean - rowMeans[i] - colMeans[j];
            }
        }

        // take the SVD of the double centred matrix, and return the
        // points from it
        var ret = numeric.svd(M),
            eigenValues = numeric.sqrt(ret.S);
        return ret.U.map((row) => {
            return numeric.mul(row, eigenValues).splice(0, dimensions);
        });
    };

    function gcd(a, b) {
        return !b ? a : gcd(b, a % b);
    };

    function lcm(a, b) {
        return (a * b) / gcd(a, b);
    };

    function leastCommonMultiple(min, max) {
        function range(min, max) {
            var arr = [];
            for (var i = min; i <= max; i++) {
                arr.push(i);
            }
            return arr;
        }

        var multiple = min;
        var pt = range(min, max).forEach(function(n) {
            multiple = lcm(multiple, n);
        });

        return multiple;
    };

    function coeffArray(size)
    {
        var coeffs = [];
        var nom = 1;
        var den = 1;
        var maxden;
        do {
            coeffs.push(nom / den);

            maxden = den;

            nom = nom + 1;

            if (nom >= den) {
                nom = 1;
                den = den + 1;
            }

            while ( gcd(den,nom) !== 1) {
                nom = nom + 1;
            }
        } while (coeffs.length < size)

        return {arr: coeffs, max: maxden}
    };
})();